import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {Container, Button} from 'react-bootstrap';
import illustration from './../../assets/img/illustration.png';

class index extends Component {
  render(){
    return(
      <Container className="homepage">
        <h1 className="homepage__title">Animanga Quiz</h1>
        <img src={illustration} alt="illustration"/>
        <Link to="/quiz">
          <Button 
            color="primary"
            size="lg"
            block>
            Start
          </Button>
        </Link>
      </Container>
    )
  }
}

export default index;
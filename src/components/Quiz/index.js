import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {Container, Form} from 'react-bootstrap';
import {connect} from 'react-redux';

import {getQuiz} from './../../store/actions/quizAction';

class index extends Component {

  componentDidMount(){
    this.props.getQuiz();
  }

  render(){
    const questionList = this.props.quiz.map((item,i) => {
      return(
        <div>
          {item.question}
          <Form>
            {
              [...item.incorrect_answers, item.correct_answer].map(option => {
                return(
                  <Form.Check
                    custom
                    type="radio"
                    label="radioo"
                    id={i}
                    name={i}
                    />
                )
              })
            }
          </Form>
        </div>
      )
    })

    return(
      <Container className="text-center mt-5">
        {questionList}
      </Container>
    )
  }
}

const mapStateToProps = store => {
  return {
    quiz: store.quizReducer.quiz
  }
}

export default connect(mapStateToProps, {getQuiz})(index);
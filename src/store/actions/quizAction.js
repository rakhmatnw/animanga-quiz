import axios from 'axios';

export const getQuiz = () => (dispatch, getState) => {
  return axios({
    method: 'GET',
    url: 'https://opentdb.com/api.php?amount=10&category=31&difficulty=easy&type=multiple'
  })
  .then(res => {
    dispatch({
      type: 'GET_QUIZ',
      payload: res.data.results
    })
  })
}
const initState = {
  quiz: []
}

export const quizReducer = (state = initState, action) => {
  if(action.type === 'GET_QUIZ'){
    return {
      ...state,
      quiz: action.payload
    }
  }
  return state
}